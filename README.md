# The Plant Monitor

## Contributors of this project 

Omolemo Kegakilwe - KGKOMO001 \
Silindokuhle Msomi - MSMLUY006 \
Matshego Kgafela - KGFMAT002

## Description of the Project 

Luya is an Engineering student who wants to grow flowers but doesn't have the time to constantly monitor a new garden. He wants \
to automate this process. 


Due to staying in a small student apartment his garden will be indoors. Therefore, he would like an environment sensor that can \
monitor the temparature, humidity and sunlight of the room the garden is in. He would like to create a single Hardware Attached on Top (HAT) for the STM32F0 microcontroller that can perform all these processes.


The HAT will be devided into 3 main modules, all with thier sub modules. These modules are: 

1. Temparature sensing module 
2. Humidity sensing module 
3. Sunlight sensing module 





